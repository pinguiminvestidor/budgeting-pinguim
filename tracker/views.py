# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

# Create your views here.

def summary(request):
    '''
    Renders the default summary dashboard to people.
    '''
    return HttpResponse('Looking for the <a href="/admin">admin</a> page?')
