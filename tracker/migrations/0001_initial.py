# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-02 13:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=255)),
                ('day', models.DateField(default=django.utils.timezone.now)),
                ('cost', models.IntegerField()),
                ('category', models.CharField(choices=[('FO', 'Food (Supermarket)'), ('ME', 'Meal'), ('TR', 'Transportation'), ('MA', 'Material Purchases'), ('FU', 'Fun'), ('BI', 'Bills'), ('OT', 'Others')], max_length=2)),
            ],
        ),
    ]
