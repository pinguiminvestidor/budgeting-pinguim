# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-01-08 12:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracker', '0010_expense_credit_card'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='expense',
            name='credit_card',
        ),
        migrations.AddField(
            model_name='expense',
            name='fulfillment',
            field=models.IntegerField(default=0, help_text='Between -1 and 1'),
        ),
    ]
