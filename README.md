# budgeting-pinguim

Sistema de gestão orçamentária do [Pinguim Investidor](https://pinguiminvestidor.com)

Requer Python 2.7, Django, e muita disposição nerd para funcionar.

## Instruções de instalação

Este sistema roda nativamente no Linux por conta da ubiquidade do Python na maioria das distribuições, mas requer o pacote Django para funcionar, preferencialmente se isolado via `virtualenv`.

Instruções cruas:

 - Instale Python
 - Instale `virtualenv`
 - Crie um ambiente isolado com: `virtualenv meuambiente`
 - Ative o embiente: `cd meuambiente; source bin/activate`
 - Instale o Django: `pip install django`
 - Copie o conteúdo deste repositório para `meuambiente`
 - Crie o banco de dados: `./manage.py makemigrations; ./manage.py migrate`
 - Inicie a aplicação: `./manage.py runserver`
 - Acesse o sistema em http://localhost:8000

É isso aí... informações bem cruas por enquanto porque eu priorizo o tempo pra ganhar dinheiro, não desenvolver.

Abraços e seguimos em frente!
